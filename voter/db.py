from sqlalchemy import select
from sqlalchemy import ForeignKey
from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy import UniqueConstraint

from sqlalchemy.orm import Mapped
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import relationship
from sqlalchemy.orm import mapped_column

from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import async_sessionmaker


class Base(AsyncAttrs, DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]


class Survey(Base):
    __tablename__ = "survey"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    desc: Mapped[str] = mapped_column(nullable=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))


class Alternative(Base):
    __tablename__ = "alternative"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    __table_args__ = (
        UniqueConstraint("name"),
    )


class SurveyAlternative(Base):
    __tablename__ = "survey_alternative"

    survey_id: Mapped[int] = mapped_column(ForeignKey("survey.id"))
    alternative_id: Mapped[int] = mapped_column(ForeignKey("alternative.id"))

    survey = relationship("Survey")
    alternative = relationship("Alternative")

    __table_args__ = (
        PrimaryKeyConstraint("survey_id", "alternative_id"),
    )


class SurveyAlternativeUserRank(Base):
    __tablename__ = "survey_alternative_user_rank"

    survey_id: Mapped[int] = mapped_column(ForeignKey("survey.id"))
    alternative_id: Mapped[int] = mapped_column(ForeignKey("alternative.id"))
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    rank: Mapped[int] = mapped_column(nullable=False)

    survey = relationship("Survey")
    alternative = relationship("Alternative")
    user = relationship("User")

    __table_args__ = (
        PrimaryKeyConstraint("survey_id", "alternative_id", "user_id"),
    )
    

async def select_all_surveys(
    async_session: async_sessionmaker[AsyncSession],
) -> list:
    async with async_session() as session:
        stmt = select(Survey)
        rslt = await session.execute(stmt)
        return rslt.scalars()
    

async def select_alternative_by_name(
    name: str, async_session: async_sessionmaker[AsyncSession],
) -> Alternative:
     async with async_session() as session:
        stmt = select(Alternative).where(Alternative.name == name)
        rslt = await session.execute(stmt)
        return rslt.first()
     

async def select_alternative_by_id(
    id: str, async_session: async_sessionmaker[AsyncSession],
) -> Alternative:
     async with async_session() as session:
        stmt = select(Alternative).where(Alternative.id == id)
        rslt = await session.execute(stmt)
        return rslt.scalar_one()
     

async def select_alternatives_by_survey_id(
    survey_id: str, async_session: async_sessionmaker[AsyncSession],
) -> Alternative:
     async with async_session() as session:
        stmt = select(SurveyAlternative.alternative_id).where(SurveyAlternative.survey_id == survey_id)
        rslt = await session.execute(stmt)
        return rslt.scalars()
    

async def is_user_exists(
    user: User, async_session: async_sessionmaker[AsyncSession],
) -> bool:
    async with async_session() as session:
        stmt = select(User).filter_by(id=user.id)
        rslt = await session.execute(stmt)
        return bool(rslt.first())
    

async def insert_one(
    obj: Base, async_session: async_sessionmaker[AsyncSession], return_id: bool = False
) -> [int, None]:
    async with async_session() as session:
        async with session.begin():
            session.add(obj)
            await session.commit()
            if return_id:
                # await session.flush()
                # await session.refresh(obj)
                return obj.id
