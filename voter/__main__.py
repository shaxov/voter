import os
import yaml
import codecs
import logging
import argparse
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio import async_sessionmaker

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from voter import db

os.chdir(os.path.dirname(__file__))

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO,
)
# Define logger
logger = logging.getLogger(__name__)

# Define states
(
    SELECT_ACTION, 
    PASS_SURVEY, 
    CREATE_SURVEY, 
    RETURN_RESULTS, 
    START_OVER, 
    STOPPING, 
    SHOWING,
    NO_SURVEYS,
) = map(chr, range(8))
(
    ENGINE,
    ASYNC_SESSION,
) = map(chr, range(8, 10))
(
    USER_ID,
    SURVEY_ID,
    ALTERNATIVES_ID,
    SURVEY_NAME,
    SURVEY_DESCRIPTION,
    SURVEY_ALTERNATIVES,
    ADD_SURVEY_NAME,
    ADD_SURVEY_DESCRIPTION,
    ADD_SURVEY_ALTERNATIVES,
    ADD_SURVEY_TO_DATABASE,
)  = map(chr, range(10, 20))

(
    SHOW_ALTERNATIVES,
    SHOW_RANKED_ALTERNATIVES,
    ALTERNATIVE_IDS,
    ALTERNATIVE_NAMES,
    ALTERNATIVE_RANKS,
    ADD_RANKED_ALTERNATIVES_TO_DATABASE,
) = map(chr, range(20, 26))

END = ConversationHandler.END

with codecs.open("configs/ukr.yml", 'r') as file:
    CONFIG = yaml.safe_load(file)

engine = None
async_session = None

# Define handlers
# Top level conversation callbacks
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """Select an action: Pass survey, Return survey results or Create survey."""
    global engine, async_session

    cfg = CONFIG["start"]
    text = cfg["text"]["select"]

    # Create database if not created
    if engine is None:
        engine = create_async_engine('sqlite+aiosqlite:///database.sqlite', echo=True)
        async_session = async_sessionmaker(engine, expire_on_commit=False)
        async with engine.begin() as conn:
            await conn.run_sync(db.Base.metadata.create_all)
    
    # Add user to database if not exists
    if update.message:
        user = update.message.from_user
        context.user_data[USER_ID] = user.id
        db_user = db.User(id=user.id, name=user.username)
        if not await db.is_user_exists(db_user, async_session):
            await db.insert_one(db_user, async_session)

    buttons = [
        [
            InlineKeyboardButton(text=cfg["buttons"]["pass_survey"], callback_data=str(PASS_SURVEY)),
            InlineKeyboardButton(text=cfg["buttons"]["create_survey"], callback_data=str(CREATE_SURVEY)),
        ],
        [
            InlineKeyboardButton(text=cfg["buttons"]["return_results"], callback_data=str(RETURN_RESULTS)),
            InlineKeyboardButton(text=cfg["buttons"]["finish"], callback_data=str(END)),
        ],
    ]
    keyboard = InlineKeyboardMarkup(buttons)

    # If we're starting over we don't need to send a new message
    if context.user_data.get(START_OVER):
        await update.callback_query.answer()
        await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
    else:
        await update.message.reply_text(cfg["text"]["greeting"])
        await update.message.reply_text(text=text, reply_markup=keyboard)

    context.user_data[START_OVER] = False
    context.user_data[ENGINE] = engine
    context.user_data[ASYNC_SESSION] = async_session

    return SELECT_ACTION


async def pass_survey(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Start pass survey conversation.  """
    cfg = CONFIG["pass_survey"]
    async_session = context.user_data[ASYNC_SESSION]

    await update.callback_query.answer()

    surveys = await db.select_all_surveys(async_session)

    # Show surveys from the data base if it is not empty.
    if not surveys:
        buttons = [[
            InlineKeyboardButton(text=cfg["buttons"]["create_survey"], callback_data=str(CREATE_SURVEY)),
            InlineKeyboardButton(text=cfg["buttons"]["back"], callback_data=str(END)),
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

        await update.callback_query.edit_message_text(text=cfg["text"]["empty"], reply_markup=keyboard)
        context.user_data[START_OVER] = True

        return NO_SURVEYS
    else:
        buttons = []
        for survey in surveys:
            buttons.append([InlineKeyboardButton(text=survey.name, callback_data=str(SHOW_ALTERNATIVES) + f":{survey.id}")])
        keyboard = InlineKeyboardMarkup(buttons)

        await update.callback_query.edit_message_text(text=cfg["text"]["select"], reply_markup=keyboard)
        context.user_data[START_OVER] = True

        return SHOW_ALTERNATIVES
    

async def show_alternatives(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Show alternatives. """
    cfg = CONFIG["show_alternatives"]

    survey_id = int(update.callback_query.data.split(":")[-1])
    context.user_data[SURVEY_ID] = survey_id

    async_session = context.user_data[ASYNC_SESSION]
    # TODO: Ask user if he wants to pass again the survey.
    alternatives = await db.select_alternatives_by_survey_id(survey_id, async_session)
    alt_names, alt_ids = [], []
    for alternative_id in alternatives:
        alternative = await db.select_alternative_by_id(alternative_id, async_session)
        alt_names.append(alternative.name)
        alt_ids.append(alternative.id)

    context.user_data[ALTERNATIVE_IDS] = alt_ids
    context.user_data[ALTERNATIVE_NAMES] = alt_names

    text = cfg["text"]["enter_ranks"] % "\n\t".join([f"{i + 1}. {name}" for i, name in enumerate(alt_names)])
    
    await update.callback_query.answer()
    await update.callback_query.edit_message_text(text=text)
    
    return SHOW_RANKED_ALTERNATIVES


async def show_ranked_alternatives(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Show alternatives. """
    cfg = CONFIG["show_ranked_alternatives"]
    
    alt_names = context.user_data[ALTERNATIVE_NAMES]

    try:
        alt_ranks = [int(alt_rank) for alt_rank in update.message.text.split(",")]
    except ValueError:
        await update.message.reply_text(text=cfg["text"]["int_error"])
        return await incorrect_ranked_alternatives(update, context)

    if len(set(alt_ranks)) != len(alt_ranks):
        await update.message.reply_text(text=cfg["text"]["unique_error"])
        return await incorrect_ranked_alternatives(update, context)
    
    if len(alt_names) != len(alt_ranks):
        await update.message.reply_text(text=cfg["text"]["count_error"])
        return await incorrect_ranked_alternatives(update, context)

    context.user_data[ALTERNATIVE_RANKS] = alt_ranks

    text = cfg["text"]["ranks_check"] % "\n\t".join([f"{i + 1}. {alt_names[rank - 1]}" for i, rank in enumerate(alt_ranks)])
    
    buttons = [
        [
            InlineKeyboardButton(text=cfg["buttons"]["alternative_ranks_ok"], callback_data=str(ADD_RANKED_ALTERNATIVES_TO_DATABASE)),
            InlineKeyboardButton(text=cfg["buttons"]["alternative_ranks_redo"], callback_data=str(SHOW_ALTERNATIVES)),
        ],
        [
            InlineKeyboardButton(text=cfg["buttons"]["return_to_menu"], callback_data=str(END)),
        ],
    ]
    keyboard = InlineKeyboardMarkup(buttons)

    await update.message.reply_text(text=text, reply_markup=keyboard)
    context.user_data[START_OVER] = True

    return ADD_RANKED_ALTERNATIVES_TO_DATABASE


async def add_ranked_alternatives_to_database(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["add_ranked_alternatives_to_database"]

    alt_ids = context.user_data[ALTERNATIVE_IDS]
    alt_ranks = context.user_data[ALTERNATIVE_RANKS]
    user_id = context.user_data[USER_ID]
    async_session = context.user_data[ASYNC_SESSION]

    survey_id = context.user_data[SURVEY_ID]
    for alt_id, alt_rank in zip(alt_ids, alt_ranks):
        await db.insert_one(
            db.SurveyAlternativeUserRank(
                survey_id=survey_id,
                alternative_id=alt_id,
                user_id=user_id,
                rank=alt_rank,
            ),
            async_session,
        )
        logger.info(f"Survey-Alternative-User link = {survey_id}-{alt_id}-{user_id} was added to database with rank = {alt_rank}.")

    buttons = [
        [
            InlineKeyboardButton(text=cfg["buttons"]["return_to_menu"], callback_data=str(END)),
        ],
    ]
    keyboard = InlineKeyboardMarkup(buttons)

    await update.callback_query.answer()
    await update.callback_query.edit_message_text(text=cfg["text"]["thanks"], reply_markup=keyboard)

    return END

async def incorrect_ranked_alternatives(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Show alternatives. """
    cfg = CONFIG["incorrect_ranked_alternatives"]

    alt_names = context.user_data[ALTERNATIVE_NAMES]
    text = cfg["text"]["retype"] % "\n\t".join([f"{i + 1}. {name}" for i, name in enumerate(alt_names)])

    await update.message.reply_text(text=text)
    
    return SHOW_RANKED_ALTERNATIVES


async def create_survey(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Starts create survey conversation. """
    cfg = CONFIG["create_survey"]

    await update.callback_query.answer()
    await update.callback_query.edit_message_text(text=cfg["text"]["enter_name"])

    return ADD_SURVEY_NAME


async def add_survey_name(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    """ Adds survey name to user context. """
    cfg = CONFIG["add_survey_name"]

    context.user_data[SURVEY_NAME] = update.message.text

    await update.message.reply_text(cfg["text"]["enter_description"])

    return ADD_SURVEY_DESCRIPTION


async def add_survey_description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["add_survey_description"]

    context.user_data[SURVEY_DESCRIPTION] = update.message.text

    await update.message.reply_text(cfg["text"]["enter_alternatives"])

    return ADD_SURVEY_ALTERNATIVES


async def skip_survey_description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["add_survey_description"]

    context.user_data[SURVEY_DESCRIPTION] = ""

    await update.message.reply_text(cfg["text"]["enter_alternatives"])

    return ADD_SURVEY_ALTERNATIVES


async def add_survey_alternatives(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["add_survey_alternatives"]

    text = update.message.text

    survey_alts = [word.strip().lower() for word in text.split(',')]
    if len(set(survey_alts)) != len(survey_alts):
        await update.message.reply_text(text=cfg["text"]["unique_error"])
        return await incorrect_survey_alternatives(update, context)

    context.user_data[SURVEY_ALTERNATIVES] = survey_alts

    buttons = [
        [
            InlineKeyboardButton(text=cfg["buttons"]["survey_ok"], callback_data=str(ADD_SURVEY_TO_DATABASE)),
            InlineKeyboardButton(text=cfg["buttons"]["survey_redo"], callback_data=str(CREATE_SURVEY)),
        ],
        [
            InlineKeyboardButton(text=cfg["buttons"]["return_to_menu"], callback_data=str(END)),
        ],
    ]
    keyboard = InlineKeyboardMarkup(buttons)

    text = cfg["text"]["check_survey"]
    text += cfg["text"]["desc_survey"] % (
        context.user_data[SURVEY_NAME],
        context.user_data[SURVEY_DESCRIPTION],
        "\n\t".join([f"{i + 1}. {alt}" for i, alt in enumerate(survey_alts)])
    )

    await update.message.reply_text(text=text, reply_markup=keyboard)
    context.user_data[START_OVER] = True
    
    return ADD_SURVEY_TO_DATABASE

async def incorrect_survey_alternatives(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["incorrect_survey_alternatives"]

    await update.message.reply_text(text=cfg["text"]["retype"])
    
    return ADD_SURVEY_ALTERNATIVES


async def add_survey_to_database(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    cfg = CONFIG["add_survey_to_database"]

    async_session = context.user_data[ASYNC_SESSION]

    # Add survey
    surevy = db.Survey(
        name=context.user_data[SURVEY_NAME], 
        desc=context.user_data[SURVEY_DESCRIPTION],
        user_id=context.user_data[USER_ID])
    survey_id = await db.insert_one(surevy, async_session, return_id=True)
    logger.info(f"Survey with id = {survey_id} was added to data base.")

    # Add survey alternatives
    alternatives = []
    for name in context.user_data[SURVEY_ALTERNATIVES]:
        alt = await db.select_alternative_by_name(name, async_session)
        if not alt:
            alt = db.Alternative(name=name)
            alt_id = await db.insert_one(
                alt, async_session, return_id=True)
            logger.info(f"Alternative with id = {alt_id} was added to data base.")
        else:
            alt_id = alt[0].id
            logger.info(f"Alternative with id = {alt_id} already exists in database.")
        alternatives.append(alt_id)

    # Add links between survey and its alternatives
    for alt_id in alternatives:
        surv_alt = db.SurveyAlternative(survey_id=survey_id, alternative_id=alt_id)
        await db.insert_one(surv_alt, async_session)
        logger.info(f"Survey-Alternative link {survey_id}-{alt_id} was added to data base.")

    buttons = [
        [
            InlineKeyboardButton(text=cfg["buttons"]["return_to_menu"], callback_data=str(END)),
        ],
    ]
    keyboard = InlineKeyboardMarkup(buttons)

    await update.callback_query.answer()
    await update.callback_query.edit_message_text(text=cfg["text"]["thanks"], reply_markup=keyboard)

    return END


async def return_results(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
    await update.callback_query.answer()

    return END


async def end(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """End conversation from InlineKeyboardButton."""
    cfg = CONFIG["end"]
    await update.callback_query.answer()
    await update.callback_query.edit_message_text(text=cfg["text"]["goodbye"])

    return END


async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """End Conversation by command."""
    cfg = CONFIG["end"]
    await update.message.reply_text(text=cfg["text"]["goodbye"])

    return END


async def stop_nested(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """End nested Conversation by command."""
    cfg = CONFIG["stop_nested"]
    await update.message.reply_text(text=cfg["text"]["goodbye"])

    return STOPPING



def main(args) -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(args.token).build()
    
    create_survey_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(create_survey, pattern="^" + str(CREATE_SURVEY) + "$")],
        states={
            ADD_SURVEY_NAME: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, add_survey_name),
            ],
            ADD_SURVEY_DESCRIPTION: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, add_survey_description),
                CommandHandler("skip", skip_survey_description),
            ],
                
            ADD_SURVEY_ALTERNATIVES: [
                MessageHandler(filters.Regex("[,\s]+"), add_survey_alternatives),
                # MessageHandler(filters.TEXT & ~filters.COMMAND, add_survey_alternatives),
            ],
            ADD_SURVEY_TO_DATABASE: [
                CallbackQueryHandler(create_survey, pattern="^" + str(CREATE_SURVEY) + "$"),
                CallbackQueryHandler(add_survey_to_database, pattern="^" + str(ADD_SURVEY_TO_DATABASE) + "$"),
                CallbackQueryHandler(start, pattern="^" + str(END) + "$"),
            ],
        },
        fallbacks=[
            MessageHandler(filters.TEXT & ~filters.COMMAND, incorrect_survey_alternatives),
            CommandHandler("stop", stop_nested),
        ],
        map_to_parent={
            END: STOPPING,
            STOPPING: STOPPING,
            SELECT_ACTION: SELECT_ACTION,
        },
    )
    pass_survey_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(pass_survey, pattern="^" + str(PASS_SURVEY) + "$")],
        states={
            SHOW_ALTERNATIVES: [
                CallbackQueryHandler(show_alternatives, pattern="^" + str(SHOW_ALTERNATIVES) + ":[0-9]+$"),
            ],
            SHOW_RANKED_ALTERNATIVES: [
                MessageHandler(filters.Regex("[,\s]+"), show_ranked_alternatives),
            ],
            ADD_RANKED_ALTERNATIVES_TO_DATABASE: [
                CallbackQueryHandler(show_alternatives, pattern="^" + str(SHOW_ALTERNATIVES) + "$"),
                CallbackQueryHandler(add_ranked_alternatives_to_database, pattern="^" + str(ADD_RANKED_ALTERNATIVES_TO_DATABASE) + "$"),
                CallbackQueryHandler(start, pattern="^" + str(END) + "$"),
            ],
        },
        fallbacks=[
            MessageHandler(filters.TEXT & ~filters.COMMAND, incorrect_ranked_alternatives),
            CommandHandler("stop", stop_nested),
        ],
        map_to_parent={
            END: STOPPING,
            STOPPING: STOPPING,
            SELECT_ACTION: SELECT_ACTION,
        },
    )
    selection_handlers = [
        pass_survey_handler,
        create_survey_handler,
        CallbackQueryHandler(return_results, pattern="^" + str(RETURN_RESULTS) + "$"),
        CallbackQueryHandler(end, pattern="^" + str(END) + "$"),
    ]
    no_surveys_handlers = [
        CallbackQueryHandler(start, pattern="^" + str(END) + "$"),
        create_survey_handler,
    ]
    stopping_handlers = [
        CommandHandler("start", start), 
        CallbackQueryHandler(start, pattern="^" + str(END) + "$"),
    ]

    # Set up top level ConversationHandler
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            SELECT_ACTION: selection_handlers,
            NO_SURVEYS: no_surveys_handlers,
            STOPPING: stopping_handlers,
        },
        fallbacks=[CommandHandler("stop", stop)],
    )

    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--token", type=str, default="6409198894:AAENIfTFesyJn_1U0wz9Twt8bJLMzBH88OA")
    main(args=parser.parse_args())