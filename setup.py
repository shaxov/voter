import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="voter",
    version="0.1",
    author="Maksym Shpakovych",
    author_email="maksym.shpakovych@gmain.com",
    description="Telegram bot for voting.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/shaxov/voter",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
